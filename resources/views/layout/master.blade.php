<html>
<head>
  <link rel="shortcut icon" href="img/shortcut-logo.png" />
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
  <link href="{{ asset('css/style.css') }}" rel="stylesheet">
  <script src="{{ asset('js/jquery-3.1.1.min.js') }}" charset="utf-8"></script>
  <script src="{{ asset('js/bootstrap.min.js') }}" charset="utf-8"></script>
  <script src="{{ asset('js/custom.js') }}" charset="utf-8"></script>
  <script src="{{ asset('js/backtotop.js') }}" charset="utf-8"></script>
  <title>PICL Institute @yield('title')</title>
</head>
<body>
  <a href="#" id="back-to-top" title="Back to top" ><img src='img/Back2Top .png' /></a>
  @include('includes.header')<br>
  @include('includes.menu')
  @include('includes.slide')<br>
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 left" style="background-color:#2E3192">
        @include('includes.leftmenu')

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="background-color:#FFFFFF">
        @yield('content')
        </div>
        @include('includes.right')

      </div></div></div><br>
      @include('includes.footer')
    </body>
    </html>
