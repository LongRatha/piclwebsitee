<div class="container-fluid">
  <div class="row" style="background-color:#2E3192; min-height:100px; color:#FFFFFF;">
    <div class="container">
      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding:0px;">
        <img src="img/picl_logo.png" width="300px" height="65px" class="img-responsive" style="margin:20px 0px;">
        <!--<h4>Professional Institue of Computer & Language</h4>-->
        <ul style="list-style-type:none;" class="out">
          <li><a href="{{url('/')}}" ><span>HOME</span></a></li>
          <li><a href="{{url('/program')}}"><span>Program</span></a></li>
          <li><a href="{{url('/news')}}"><span>News & Event</span></a></li>
          <li><a href="{{url('/information')}}"><span>Information</span></a></li>
          <li><a href="{{url('/career')}}"><span>Career</span></a></li>
          <li><a href="{{url('/contact')}}"><span>Contact Us</span></a></li>
          <li><a href="{{url('/about')}}"><span>About</span></a></li>
        </ul>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <center><h3>Join mail with us</h3>
        <div class="input-group">
          <input type="text" class="form-control" placeholder="Search for..." style="border-radius:0px">
          <span class="input-group-btn">
            <button class="btn btn-info" type="button" style="border-radius:0px">Summit</button>
          </span>
        </div>
        <div class="img-responsive">
          <h3>Social Network</h3>
          <img src='img/facebook-color.png'alt="Facebook" />
          <img src='img/color-nfo-logo.png'alt="Goolle+" />
          <img src='img/youtube-color.png'alt="Youtube" />
          <img src='img/twitter.png'  width="40px" height="40px" />
        </div>
        </center>
      </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
          <center><h3>Contact us</h3></center>
          <i class="glyphicon glyphicon-map-marker"></i><span> building 143 | st 273 sangkat Toul,sangke khan Russei Keo,Phnom Penh </span>
          <br>
          <i class="glyphicon glyphicon-earphone"></i><span> Tel: 086 96 97 65 / 012 58 99 33 / 066 23 47 77</span>
          <br>
          <i class="glyphicon glyphicon-globe"></i><span> Website: www.picl-institute.com
          </span>
          <br />
          <i class="glyphicon glyphicon-envelope"></i><span> picl.edu@yahoo.com</span>
        </div>
        <!-- /input-group -->
      </div>
    </div>
  </div>
