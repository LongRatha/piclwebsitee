<div id="accordion" style="color:#FFFFFF">
    <h2>Faculty</h2>
  <div class="panel-heading">
   <div class="panel-title">
    <i class="indicator glyphicon glyphicon-chevron-right" style="font-size:10px;"></i>
    <a data-toggle="collapse" href="#menuPanelListGroup" style="font-size:14px">Programming
    </a>
   </div>
  </div>
          <ul class="list-group collapse out" id="menuPanelListGroup">
             <li class="list-group-item librePanelListGroupItem">
                <a href="{{url('/p-professional')}}" target="_blank"> <span>Professional</span></a>
              </li>
              <li class="list-group-item librePanelListGroupItem">
                  <a href="{{url('/p-schedule')}}" target="_blank"><span>Schedule</span></a>
              </li>

          </ul>

  <div class="panel-heading librePanelHeading">
    <div class="panel-title">
      <i class="indicator glyphicon glyphicon-chevron-right" style="font-size:10px;"></i><a data-toggle="collapse" href="#menuPanelListGroup1" style="font-size:14px">Bussiness IT</a>
  </div>
  </div>
          <ul class="list-group collapse out" id="menuPanelListGroup1">
              <li class="list-group-item librePanelListGroupItem">
                  <a href="{{url('/b-professional')}}" target="blank"><span>Profesional</span></a>
              </li>
              <li class="list-group-item librePanelListGroupItem">
                  <a href="{{url('/b-schedule')}}" target="blank"><span>Schedule</span></a>
              </li>
      </ul>
  <div class="panel-heading librePanelHeading">
    <div class="panel-title">
      <i class="indicator glyphicon glyphicon-chevron-right" style="font-size:10px;"></i>
        <a data-toggle="collapse" href="#menu2PanelListGroup" style="font-size:14px">Management Information system</a>
  </div>
  </div>
          <ul class="list-group collapse out" id="menu2PanelListGroup">
              <li class="list-group-item librePanelListGroupItem">
                  <a href="{{url('/ma-professional')}}" target="blank"><span>Profesional</span></a>
              </li>
              <li class="list-group-item librePanelListGroupItem">
                  <a href="{{url('/ma-schedule')}}" target="blank"><span>Schedule</span></a>
              </li>
      </ul>
  <div class="panel-heading librePanelHeading">
    <div class="panel-title">
      <i class="indicator glyphicon glyphicon-chevron-right" style="font-size:10px;"></i>
        <a data-toggle="collapse" href="#menu2PanelListGroup1" style="font-size:14px">Networking</a>
  </div>
  </div>
          <ul class="list-group collapse out" id="menu2PanelListGroup1">
              <li class="list-group-item librePanelListGroupItem">
                  <a href="{{url('/n-professional')}}" target="blank"><span>Profesional</span></a>
              </li>
              <li class="list-group-item librePanelListGroupItem">
                  <a href="{{url('/n-schedule')}}" target="blank"><span>Schedule</span></a>
              </li>
      </ul>
  <div class="panel-heading librePanelHeading">
    <div class="panel-title">
      <i class="indicator glyphicon glyphicon-chevron-right" style="font-size:10px;"></i>
        <a data-toggle="collapse" href="#menu2PanelListGroup3" style="font-size:14px">Graphic Design</a>
  </div>
  </div>
          <ul class="list-group collapse out" id="menu2PanelListGroup3">
              <li class="list-group-item librePanelListGroupItem">
                  <a href="{{url('/g-professional')}}" target="blank"><span>Profesional</span></a>
              </li>
              <li class="list-group-item librePanelListGroupItem">
                  <a href="{{url('/g-schedule')}}" target="blank"><span>Schedule</span></a>
              </li>
      </ul>
    <div class="panel-heading librePanelHeading">
        <div class="panel-title">
            <i class="indicator glyphicon glyphicon-chevron-right" style="font-size:10px;"></i>
            <a data-toggle="collapse" href="#menu2PanelListGroup4" style="font-size:14px">Multimedia</a>
        </div>
    </div>
    <ul class="list-group collapse out" id="menu2PanelListGroup4">
        <li class="list-group-item librePanelListGroupItem">
            <a href="{{url('/mu-professional')}}" target="blank"><span>Profesional</span></a>
        </li>
        <li class="list-group-item librePanelListGroupItem">
            <a href="{{url('/mu-schedule')}}" target="blank"><span>Schedule</span></a>
        </li>
    </ul>
    <div class="panel-heading librePanelHeading">
        <div class="panel-title">
            <i class="indicator glyphicon glyphicon-chevron-right" style="font-size:10px;"></i>
            <a data-toggle="collapse" href="#menu2PanelListGroup5" style="font-size:14px">English</a>
        </div>
    </div>
    <ul class="list-group collapse out" id="menu2PanelListGroup5">
        <li class="list-group-item librePanelListGroupItem">
            <a href="{{url('/e-professional')}}" target="blank"><span>Profesional</span></a>
        </li>
        <li class="list-group-item librePanelListGroupItem">
            <a href="{{url('/e-schedule')}}" target="blank"><span>Schedule</span></a>
        </li>
    </ul>
    <div class="panel-heading librePanelHeading">
        <div class="panel-title">
            <i class="indicator glyphicon glyphicon-chevron-right" style="font-size:10px;"></i>
            <a data-toggle="collapse" href="#menu2PanelListGroup6" style="font-size:14px">Korea</a>
        </div>
    </div>
    <ul class="list-group collapse out" id="menu2PanelListGroup6">
        <li class="list-group-item librePanelListGroupItem">
            <a href="{{url('/k-professional')}}" target="blank"><span>Profesional</span></a>
        </li>
        <li class="list-group-item librePanelListGroupItem">
            <a href="{{url('/k-schedule')}}" target="blank"><span>Schedule</span></a>
        </li>
    </ul>
  </div>
  </div>
