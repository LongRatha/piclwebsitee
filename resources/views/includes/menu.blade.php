<div class="container-fluid">
<div class="row" >
   <nav class="my-navbar navbar navbar-default"​​​ data-spy="affix" data-offset-top="150" style="background-color:#2E3192;color:#FFFFFF;border:0px;border-radius:0px;">
  <!--<div class="container" >-->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:75px;">
        <!-- Brand and toggle get grouped for better mobile display -->

        <div class="navbar-header">
          <button type="button" style="border-radius:0px" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#drop" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="drop" >
          <ul class="nav navbar-nav" >
            <li class="{{ Request::path() == '/' ? 'bcolor' : '' }}" ><a href="{{url('/')}}"><span><i class="glyphicon glyphicon-home" aria-hidden="true"></i> HOME</span></a></li>
            <li class="{{ Request::path() == 'program' ? 'bcolor' : '' }}"><a href="{{url('/program')}}"><span>Program</span></a></li>
            <li class="{{ Request::path() == 'news' ? 'bcolor' : '' }}"><a href="{{url('/news')}}"><span>News & Event</span></a></li>
            <li class="{{ Request::path() == 'information' ? 'bcolor' : '' }}"><a href="{{url('/information')}}"><span>Information</span></a></li>
            <li class="{{ Request::path() == 'career' ? 'bcolor' : '' }}"><a href="{{url('/career')}}"><span>Career</span></a></li>
            <li class="{{ Request::path() == 'contact' ? 'bcolor' : '' }}"><a href="{{url('/contact')}}"><span>Contact Us</span></a></li>
            <li class="{{ Request::path() == 'about' ? 'bcolor' : '' }}"><a href="{{url('/about')}}"><span>About</span></a></li>

          </ul>
        </div>
        </div>
</nav>
</div> </div>
